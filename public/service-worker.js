importScripts("/cache-polyfill.js");

var STATIC_CACHE_NAME = "cowlar-plotter-static-V1";
var DYNAMIC_CACHE_NAME = "cowlar-plotter-dynamic-V1";
var STATIC_ASSETS = [
  "/",
  "/index.html",
  "/index.html?homescreen=1",
  "/?homescreen=1"
];

function isInArray(string, array) {
  var cachePath;
  if (string.indexOf(self.origin) === 0) {
    // request targets domain where we serve the page from (i.e. NOT a CDN)
    // console.log("matched ", string);
    cachePath = string.substring(self.origin.length); // take the part of the URL AFTER the domain (e.g. after localhost:8080)
  } else {
    cachePath = string; // store the full request (for CDNs)
  }
  return array.indexOf(cachePath) > -1;
}

function trimCache(cacheName, maxItems) {
  caches.open(cacheName).then(function(cache) {
    return cache.keys().then(function(keys) {
      if (keys.length > maxItems) {
        cache.delete(keys[0]).then(trimCache(cacheName, maxItems));
      }
    });
  });
}
self.addEventListener('message', (event) => {
  if (event.data === 'SKIP_WAITING') {
      self.skipWaiting();
  }
});

self.addEventListener("install", function(event) {
  event.waitUntil(
    caches.open(STATIC_CACHE_NAME).then(function(cache) {
      // We need to cache images, js, css, fonts, CDNs and logos etc
      return cache.addAll(STATIC_ASSETS);
    })
  );
});

self.addEventListener("fetch", function(event) {
  if (event.request.url.indexOf("/v1/") > -1) {
    // console.log("api caching", event.request.url);
    if (!navigator.onLine) {
      event.respondWith(
        caches.match(event.request).then(function(response) {
          if (response) {
            return response;
          } else {
            return fetch(event.request)
              .then(function(res) {
                return caches.open(DYNAMIC_CACHE_NAME).then(function(cache) {
                  // trimCache(DYNAMIC_CACHE_NAME, 25);
                  cache.put(event.request.url, res.clone());
                  return res;
                });
              })
              .catch(function(err) {
                return caches.open(STATIC_CACHE_NAME).then(function(cache) {
                  return cache.match("/offline");
                });
              });
          }
        })
      );
    } else {
      event.respondWith(
        caches.open(DYNAMIC_CACHE_NAME).then(function(cache) {
          return fetch(event.request).then(function(res) {
            // trimCache(DYNAMIC_CACHE_NAME, 25);
            cache.put(event.request, res.clone());
            return res;
          });
        })
      );
    }
  } else if (isInArray(event.request.url, STATIC_ASSETS)) {
    // console.log("Static caching", event.request.url);
    event.respondWith(caches.match(event.request));
  } else {
    // console.log("else caching", event.request.url);
    event.respondWith(
      caches.match(event.request).then(function(response) {
        if (response) {
          return response;
        } else {
          return fetch(event.request)
            .then(function(res) {
              return caches.open(DYNAMIC_CACHE_NAME).then(function(cache) {
                // trimCache(DYNAMIC_CACHE_NAME, 25);
                cache.put(event.request.url, res.clone());
                return res;
              });
            })
            .catch(function(err) {
              return caches.open(STATIC_CACHE_NAME).then(function(cache) {
                return cache.match("/offline");
              });
            });
        }
      })
    );
  }
});

self.addEventListener("activate", function(event) {
  // console.log("[SW] activating ...", e);
  event.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(
        keyList.map(function(key) {
          if (key !== STATIC_CACHE_NAME && key !== DYNAMIC_CACHE_NAME) {
            // console.log("Removing old cahce", key);
            return caches.delete(key);
          }
        })
      );
    })
  );
  return self.clients.claim();
});
