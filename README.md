# Full Stack - Offline Coding Exercise - Plotter App
## Project setup
NodeJs (version 10) should be installed on your system/server. Then this command will work
```
npm install
```
### Compiles and hot-reloads for development
After running the command below, it will return a serve link with a port, which you can run in your browser
```
npm run serve
```
### To Compile and minify for production, use the following command
```
npm run build
```
# About Plotter App
The Plotter App allows different shapes to be drawn as SVGs within a 250px x 250px grid.
Instructions & an input field to enter commands are on the left side of the screen, while the 250px x 250 px grid is located on the right of the screen.
Read the instructions, enter your commands and press the Draw button. 
The Plotter App on both landscape as well as portrait screen orientations. 
## Instructions to Plot SVGs
-   **Draw a Rectangle**

    **r** **X** Coordinate **Y** Coordinate **width** **height**

    e.g **r 75 75 100 100**
-   **Draw a Circle**

    **c** **CX** Coordinate **CY** Coordinate **radius*

    e.g **c 125 125 20**
-   **Draw a Polygon**

    **p C1,Y1 X2,Y2 Xn,Yn**

    e.g **p 200,10 150,190 60,10**
-   **Draw an Ellipse** - *Bonus Shape*

    **e** **CX** Coordinate **CY** Coordinate **RX** Radius **RY** Radius

    e.g **e 125 125 100 50**
## Validations
-   Height, width and radius must be greater than 0 and less than 1000.
-   Coordinates should be greater than equal to 0 and less than 1000.
-   Start and end spaces within each line will be trimed automatically.
-   An empty line command will cause an error.
-   A warning will be displayed if the shape is drawn outside the box.